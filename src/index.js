const express = require('express')
const http = require('http')
const SocketIO = require('socket.io')

const config = require('../config')
const SocketApp = require('./socket-app')

// Initiate the app, socket and database
const expressApp = express()
const server = http.createServer(expressApp)
const io = SocketIO.listen(server)

// Configure Express
expressApp.set('views', `${__dirname}/../views`)
expressApp.set('view engine', 'ejs')

expressApp.use(express.static(`${__dirname}/../public`))

expressApp.get('/', (_req, res) => {
  res.render('index', { isAdminRoute: false })
})

// Game socket app
new SocketApp(io)

/**
 * Start the app
 */
server.listen(config.port, function () {
  console.log(`Listening to port ${config.port}`)
})
