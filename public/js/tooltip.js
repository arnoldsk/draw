
// Made with a thought of using this only for color tooltips
$(() => {
    $('body').append('<div id="tooltip"></div>');
    $('*[title]').each(function() {
        $(this).data('tooltip-title', $(this).attr('title')).removeAttr('title').addClass('tooltip-title');
    });

    let tooltip = $('#tooltip');

    tooltip.css({
        position: 'fixed',
        display: 'none',
        'z-index': 100,
        'pointer-events': 'none',
        'user-select': 'none',
        background: '#333',
        color: '#fff',
        padding: '3px 5px',
        'border-radius': '5px',
    });

    $(document).on('mousemove', '.tooltip-title', function(e) {
        let title = $(this).data('tooltip-title');

        tooltip.html(title);
        tooltip.css({
            top: e.pageY - tooltip.height() / 2,
            left: e.pageX - tooltip.width() - 15,
            display: 'block',
        });
    }).on('mouseleave', '.tooltip-title', () => {
        tooltip.html('');
        tooltip.css({
            display: 'none',
        });
    });
});
