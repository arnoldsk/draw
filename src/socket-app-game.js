const helpers = require('./helpers')
const words = require('../words.json')

class SocketAppGame {
  constructor(app) {
    this.app = app
    this.currentWord = null
    this.drawInterval = null
    this.drawTimer = 100
    this.usedWords = []
    this.canGuess = false
    this._data = {}
  }

  get userCount() {
    return Object.keys(this.app.users).length
  }

  get currentDrawer() {
    const { drawer } = this._data

    return drawer && this.app.users[drawer]
  }

  isRunning() {
    return Object.keys(this._data).length > 0
  }

  update() {
    if (this.userCount >= 2) {
      if (!this.isRunning()) {
        this.start()
      } else if (this.haveAllGuessed()) {
        this.next()
      }
    } else {
      this.stop()
    }
  }

  start() {
    const users = this.app.users

    // Check for a new round
    let newRound = false
    if (!this.isRunning()) {
      newRound = true
    } else if (this._data.order.length <= 1) {
      newRound = true
    }

    // Update the word
    this.updateWord()

    // Build new data
    let data = {}

    if (newRound) {
      data.order = Object.keys(users)
      data.order.reverse()

      this.app.sendMessage('Started new round!')
    } else {
      data = this._data
      data.order.splice(0, 1)

      this.app.sendMessage('Next round!')
    }

    const drawer = data.order[0]

    data.drawer = drawer
    data.blanks = this.getWordBlanks()
    data.hints = []
    data.wordLength = this.currentWord.length

    // Reset user data
    for (const [id, user] of Object.entries(users)) {
      user.drawing = id === data.drawer
      user.guessed = false
      user.hasDrawn = false

      // Update user object
      users[id] = this.app.setUser(user)
    }

    this.app.io.emit('update', users, drawer)

    // Allow guessing
    this.canGuess = true

    // Start the game
    this.app.io.emit('game start', users, data)
    this.app.io.to(drawer).emit('game reveal word', this.currentWord)

    // Set the data
    this._data = data

    // Start timer
    this.setTimer()

    // Clear drawing
    this.app.clearStrokes()
  }

  next() {
    this.app.sendMessage(`The word was: ${this.currentWord}`)

    if (this.userCount < 2) {
      this.stop()
    } else {
      this.results()
      setTimeout(() => {
        this.start()
      }, 5000)
    }
  }

  stop() {
    // Reset all data
    this.canGuess = false
    this.currentWord = null
    this._data = {}

    // Clear timer
    this.clearTimer()

    // Display end screen
    this.app.io.emit('game end', this.app.users)

    // Clear drawing
    /** Disabled to allow drawing when joined/left alone */
    // this.app.clearStrokes()
  }

  results() {
    // Disallow guessing
    this.canGuess = false

    // Clear timer
    this.clearTimer()

    // Display results
    this.app.io.emit('game reveal word', this.currentWord)
    this.app.io.emit('game results', this.app.users)
  }

  hint() {
    if (!this.isRunning()) {
      return
    }

    // Don't hint if only 2 letters are left
    if (this.currentWord.length - 2 <= this._data.hints.length) {
      return
    }

    const available = []
    for (let i = 0; i < this.currentWord.length; i++) {
      if (
        !this._data.hints.includes(this.currentWord[i]) &&
        this.currentWord[i] !== ' '
      ) {
        available.push(i)
      }
    }

    const index = helpers.random(0, available.length - 1)
    const hint = {
      index,
      letter: this.currentWord[index],
    }

    // Reveal a letter
    this._data.hints.push(hint)
    this.app.io.emit('game reveal letter', hint)
  }

  updateWord() {
    const wordsNotUsed = helpers.arrayDiff(words, this.usedWords)

    if (wordsNotUsed.length == 1) {
      this.currentWord = wordsNotUsed[0]
      this.usedWords = []
    } else {
      this.currentWord =
        wordsNotUsed[helpers.random(0, wordsNotUsed.length - 1)]
      this.usedWords.push(this.currentWord)
    }
  }

  getWordBlanks() {
    let blanks = []

    for (let i = 0; i < this.currentWord.length; i++) {
      if (this.currentWord[i] == ' ') {
        blanks.push(i)
      }
    }

    return blanks
  }

  haveAllGuessed() {
    let guessedCount = 0

    for (const user of Object.values(this.app.users)) {
      if (user.guessed) {
        guessedCount++
      }
    }

    // -1 because not counting the drawer
    return guessedCount == this.userCount - 1
  }

  handleUserLeaving(id) {
    // Implying that checked if game is running
    if (this._data.order.includes(id)) {
      this._data.order.splice(this._data.order.indexOf(id), 1)
    }

    if (this._data.drawer === id) {
      this.next()
    } else {
      this.update()
    }
  }

  clearTimer() {
    this.drawTimer = 100
    clearInterval(this.drawInterval)
    this.app.io.emit('game timer', 0)
  }

  setTimer() {
    this.clearTimer()

    this.drawInterval = setInterval(() => {
      this.drawTimer--

      switch (this.drawTimer) {
        case 66:
        case 33:
        case 5:
          this.hint()
          break
        case 50:
        case 25:
          if (this.currentWord.length >= 8) {
            this.hint()
          }
          break
        case 80:
        case 40:
          if (this.currentWord.length >= 12) {
            this.hint()
          }
          break
        // case 50:
        case 70:
          // Check if user has drawn
          if (!this.currentDrawer.hasDrawn) {
            this.currentDrawer.skipped++

            if (this.currentDrawer.skipped >= 2) {
              // Kick the user
              this.app.sendMessage(
                `${this.currentDrawer.name} was kicked due to inactivity.`,
              )
              this.app.io.to(this._data.drawer).emit('kicked', this.app.users)
              this.app.disconnectUser(this._data.drawer)
            } else {
              this.app.sendMessage(
                `${this.currentDrawer.name} was skipped due to inactivity.`,
              )
            }

            this.drawTimer = 0
            clearInterval(this.drawInterval)
            this.next()
          }
          break
      }

      if (this.drawTimer <= 0) {
        this.drawTimer = 0
        clearInterval(this.drawInterval)
        this.next()
      }

      this.app.io.emit('game timer', this.drawTimer)
    }, 1000)
  }
}

module.exports = SocketAppGame
