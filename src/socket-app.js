const config = require('../config')
const SocketAppGame = require('./socket-app-game')

class SocketApp {
  constructor(io) {
    this.io = io

    // Static
    this.users = {}
    this.remoteAddresses = {}
    this.strokes = []

    // Initialize the game
    this.game = new SocketAppGame(this)

    // Socket handling
    io.on('connection', (socket) => {
      this.onConnection(socket)

      // Handle events
      const events = [
        ['joined', this.onJoined],
        ['disconnect', this.onDisconnect],
        ['kick user', this.onKickUser],
        ['draw', this.onDraw],
        ['resized', this.onResized],
        ['payload', this.onPayload],
        ['clear', this.onClear],
        ['message', this.onMessage],
      ]

      for (const [name, callback] of events) {
        socket.on(name, (...args) => callback.bind(this)(socket, ...args))
      }
    })
  }

  getRemoteAddress(socket) {
    const { headers } = socket.handshake
    const remoteAddress =
      typeof headers['cf-connecting-ip'] !== 'undefined'
        ? headers['cf-connecting-ip']
        : headers['x-forwarded-for']

    if (config.devMode || typeof remoteAddress === 'undefined') {
      return '127.0.0.1'
    }

    return remoteAddress
  }

  validateRemoteAddress(remoteAddress) {
    if (config.devMode || remoteAddress === '127.0.0.1') {
      return true
    }

    return !Object.values(this.remoteAddresses).includes(remoteAddress)
  }

  onConnection(socket) {
    const remoteAddress = this.getRemoteAddress(socket)

    // Prevent user from joining with the same IP address
    if (!this.validateRemoteAddress(remoteAddress)) {
      socket.emit('duplicate remote address')
      socket.disconnect()
      return
    }

    // Store the remote address
    if (!this.remoteAddresses[socket.id]) {
      this.remoteAddresses[socket.id] = remoteAddress
    }

    socket.emit('users', this.users, '')
  }

  onJoined(socket, name, code) {
    // User already exists
    if (this.users[socket.id]) {
      return
    }

    // Build the user object
    const isAdmin = code && config.codes.includes(code)

    name = name.trim().length >= 3 ? name.trim() : 'Unknown'

    const user = {
      id: socket.id,
      name,
      online: true,
      drawing: false,
      guessed: false,
      points: 0,
      skipped: 0,
      roundsToDraw: false,
      hasDrawn: false,
      isAdmin,
      joined: false,
    }

    // Send an update
    this.io.emit('update', this.users, {
      ...user,
      joined: true,
    })

    this.addUser(user)

    // Alert
    this.sendMessage(`${user.name} joined.`)

    /**
     * @todo do not use _x variable
     */
    socket.emit('init', this.users, this.game._data, this.strokes)

    // Update the game
    this.game.update()
  }

  onDisconnect(socket) {
    // Disconnect user
    if (this.users[socket.id]) {
      this.disconnectUser(socket.id)
    }

    // Delete remote address
    if (this.remoteAddresses[socket.id]) {
      delete this.remoteAddresses[socket.id]
    }
  }

  onKickUser(socket, id) {
    const user = this.users[socket.id]

    if (!user) {
      return socket.disconnect()
    }

    if (!user.isAdmin || !this.users[id]) {
      return
    }

    // Disconnect the user
    this.disconnectUser(id)

    // Alert
    this.sendMessage(`${user.name} has been kicked.`)
  }

  onDraw(socket, data) {
    const user = this.users[socket.id]

    if (!user) {
      socket.disconnect()
      return
    }

    if (this.game.isRunning() && !user.drawing) {
      return
    }

    // Send draw event to other players
    socket.broadcast.emit('draw', user, data)
  }

  onResized(socket) {
    socket.emit('resized', this.strokes)
  }

  onPayload(socket, payload) {
    const user = this.users[socket.id]

    if (!user || !payload.length) {
      return
    }

    if (this.game.isRunning() && !user.drawing) {
      return
    }

    // Update user drawn state
    this.users[socket.id].hasDrawn = true

    // Add strokes
    for (const item of payload) {
      this.strokes.push(item)
    }
  }

  onClear(socket) {
    const user = this.users[socket.id]

    if (!user) {
      socket.disconnect()
      return
    }

    if (this.game.isRunning() && !user.drawing) {
      return
    }

    // Reset strokes
    this.clearStrokes()
  }

  onMessage(socket, message) {
    const user = this.users[socket.id]

    if (!user) {
      socket.disconnect()
      return
    }

    if (
      this.game.currentWord === message.trim().toLowerCase() &&
      this.game.canGuess
    ) {
      // Prevent drawers/winners from saying the word... at least try to
      if (user.drawing || user.guessed) {
        return
      }

      // Update user state
      user.points++
      user.guessed = true

      // Update users
      this.io.emit('update', this.users, user)

      // Reveal the word for the player
      socket.emit('game reveal word', this.game.currentWord)

      // Announce player guessing the word
      this.sendMessage(`${user.name} guessed the word!`)

      // Update user object
      this.users[socket.id] = user

      // Update the game
      this.game.update()
    } else {
      // Send the message regularly
      this.sendMessage(message, user)
    }
  }

  sendMessage(message, user = null) {
    this.io.emit('message', user, message)
  }

  setUser(user) {
    const { id } = user

    this.users[id] = user

    return user
  }

  addUser(user) {
    this.setUser(user)

    return user
  }

  removeUser(user) {
    const { id } = user

    // Delete the user
    if (this.users[id]) {
      delete this.users[id]
    }

    return user
  }

  clearStrokes() {
    // Reset strokes
    this.strokes = []

    // Clear on front-end
    this.io.emit('clear')
  }

  disconnectUser(id) {
    const user = this.users[id]

    if (!user) {
      return
    }

    // Set the online state to false
    user.online = false

    // Update for all players
    this.io.emit('left', this.users, user)

    // Alert
    this.sendMessage(`${user.name} left.`)

    // Remove the player
    this.removeUser(user)

    // Update game
    if (this.game.isRunning()) {
      this.game.handleUserLeaving(id)
    }

    // Disconnect manually just in case
    if (this.io.sockets.connected[id]) {
      this.io.sockets.connected[id].disconnect()
    }
  }
}

module.exports = SocketApp
