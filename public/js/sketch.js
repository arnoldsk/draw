const brushSizeMin = 5
const brushSizeMax = 100
let brushSize = 10
let canDraw = false
let name = Cookies.get('name')
let color = '#000000'
let isAdmin = false
let img
let drawColors = true

init()

const socket = io.connect('', {
  transports: ['websocket'],
})

let _lastMouseX
let _lastMouseY
let _payload = []
let _lastColor = null

socket.on('duplicate remote address', () => {
  showOverlay('duplicate-remote-address')
})

// Outside setup because it's a DOM update
socket.on('users', (users, name) => {
  if (name.length) {
    $('#name').val(name)
    checkCodeData()
    $('#submit').slideDown(300)
  }

  for (const user of Object.values(users)) {
    $('#names').find(`.user-${user.id}`).remove()
    let name = $('<div class="name">')
    name.appendTo('#names')
    name.html(
      `<span class="name-name">${user.name}</span> <div class="user-info">${
        user.points
      } ${plural('point', user.points)}</div>`,
    )
    name.addClass(`user-${user.id}`)

    if (user.isAdmin) {
      name.addClass('admin')
    }
    if (isAdmin && !user.isAdmin) {
      name.append(
        `<a href="javascript:void(0);" class="kick-user" data-user="${user.id}">kick</a>`,
      )
    }
  }
})

function setup() {
  // Since there's only one drawer, I can afford using 60fps
  frameRate(60)
  createCanvas(windowWidth, windowHeight)
  background('#f6f6f6')

  img = loadImage('img/colors.jpg')

  socket.on('update', (users, user) => {
    if (user.id === socket.id && user.isAdmin) {
      isAdmin = true
    }

    for (const user of Object.values(users)) {
      $('#names').find(`.user-${user.id}`).remove()

      const name = $('<div class="name">')
      name.appendTo('#names')
      name.html(
        `<span class="name-name">${user.name}</span> <div class="user-info">${
          user.points
        } ${plural('point', user.points)}</div>`,
      )
      name.addClass(`user-${user.id}`)

      if (user.joined) {
        name.addClass('joined')
        setTimeout(() => {
          name.removeClass('joined')
        }, 1000)
      }

      if (user.drawing) {
        updateUserStatus(user.id, 'Drawing')
      } else {
        updateUserStatus(user.id, null)
      }

      if (user.guessed) {
        name.addClass('guessed')
      } else {
        name.removeClass('guessed')
      }

      if (user.isAdmin) {
        name.addClass('admin')
      }

      if (isAdmin && !user.isAdmin) {
        name.append(
          `<a href="javascript:void(0);" class="kick-user" data-user="${user.id}">kick</a>`,
        )
      }
    }
  })

  socket.on('init', (users, game, strokes) => {
    render(strokes)
    updateHeading(users, game)
  })

  socket.on('left', (users, user) => {
    let name = $('#names').find(`.user-${user.id}`)
    name.addClass('left')
    setTimeout(() => {
      name.remove()
    }, 1000)

    $('#names').append(name)
  })

  socket.on('kicked', (users, user) => showOverlay('kicked'))

  socket.on('draw', (user, data) => {
    fill(data.color)
    noStroke()
    ellipse(
      (data.x2 * windowWidth) / 100,
      (data.y2 * windowHeight) / 100,
      data.brushSize,
      data.brushSize,
    )

    if (data.x1 && data.y1) {
      stroke(data.color)
      strokeWeight(data.brushSize)
      line(
        (data.x1 * windowWidth) / 100,
        (data.y1 * windowHeight) / 100,
        (data.x2 * windowWidth) / 100,
        (data.y2 * windowHeight) / 100,
      )
    }
  })

  socket.on('resized', (strokes) => render(strokes))

  $(document).on('click', '#clear', () => {
    if (!canDraw) return false

    $('#clear').prop('disabled', true)
    socket.emit('clear')
  })

  socket.on('clear', () => {
    $('#clear').prop('disabled', false)
    background('#f6f6f6')
  })

  $(document).on('keyup', '#message', (e) => {
    let message = $('#message').val().trim()

    if (e.keyCode === 13 && message.length) {
      socket.emit('message', message)
      $('#message').val('')
    }
  })

  socket.on('message', (user, message) => {
    // Remove old messages
    let messageCount = $('#messages .message').length
    if (messageCount >= 50) {
      $('#messages .message:lt(' + (messageCount - 50) + ')').remove()
    }

    let element = $('<div class="message">')
    if (user) {
      element.html(`<strong>${user.name}</strong>: ${message}</div>`)
      if (user.guessed) element.addClass('guessed')
    } else {
      element.addClass('non-user')
      element.html(`<strong>${message}</strong>`)
    }
    element.emoji()
    $('#messages').append(element).scrollTop($('#messages')[0].scrollHeight)
  })

  $(document).on('click', '#colors .color', ({ target }) => {
    if (!canDraw) {
      return
    }

    _lastColor = null
    color = $(target).data('color')

    $('#guide').css('border-color', color)

    $('#colors .color.selected').removeClass('selected')
    $(target).addClass('selected')
  })

  $(document).on('change', '#brush-size', () => {
    updateBrushSize($('#brush-size').val())
  })

  socket.on('game start', (users, game) => {
    updateHeading(users, game)
    if (game.drawer === socket.id) {
      let audio = new Audio('audio/alert.mp3')
      audio.volume = 0.3
      audio.play()
    }
  })

  socket.on('game reveal word', (word) => {
    setTimeout(() => {
      for (let i = 0; i < word.length; i++) {
        $('#heading .letter').eq(i).html(word[i])
      }
    }, 500)
  })

  socket.on('game reveal letter', (hint) => {
    $('#heading .letter').eq(hint.index).html(hint.letter)
  })

  socket.on('game timer', (seconds) => {
    $('#timer').html(seconds)

    if (seconds <= 0) {
      $('#timer').html('')
    } else if (seconds > 70) {
      $('#timer').html(`
        ${seconds}
        <small>You will be kicked if you don't draw within 30 seconds.</small>
      `)
    }
  })

  socket.on('game end', (users) => {
    $('#names .status').remove()
    $('#heading').html('Waiting for another player...')
  })

  socket.on('game results', (users) => {
    let results = $('<div id="results">')
    let sortedUserIds = sortUsersByPoints(users)

    for (let i = 0; i < sortedUserIds.length; i++) {
      let id = sortedUserIds[i]

      // Show people who did not draw
      if (!users[id].drawing) {
        let userScore = users[id].guessed
          ? '<span class="positive">+1</span>'
          : '<span class="negative">+0</span>'
        results.append(
          `<div class="result"><strong>${users[id].name}</strong> ${userScore}</div>`,
        )
      }
    }
    $('#heading').append(results)
  })

  $(document).on('click', '.kick-user', ({ target }) => {
    const id = $(target).data('user')

    if (confirm('Are you sure?')) {
      socket.emit('kick user', id)
    }
  })

  $(document).on('click', '#save', () => {
    drawColors = false
    setTimeout(saveCanvas, 500)
    setTimeout(() => {
      drawColors = true
    }, 1000)
  })
}

function render(strokes) {
  if (!strokes.length) {
    return
  }

  background('#f6f6f6')

  for (const data of strokes) {
    fill(data.color)
    noStroke()
    ellipse(
      (data.x2 * windowWidth) / 100,
      (data.y2 * windowHeight) / 100,
      data.brushSize,
      data.brushSize,
    )

    if (data.x1 && data.y1) {
      stroke(data.color)
      strokeWeight(data.brushSize)
      line(
        (data.x1 * windowWidth) / 100,
        (data.y1 * windowHeight) / 100,
        (data.x2 * windowWidth) / 100,
        (data.y2 * windowHeight) / 100,
      )
    }
  }
}

function draw() {
  if (!canDraw) {
    return
  }

  if (mouseIsPressed) {
    // Only if mouse is on the canvas
    let hoverElement = document.elementFromPoint(mouseX, mouseY)
    if ((hoverElement && hoverElement.tagName !== 'CANVAS') || !hoverElement)
      return

    $('#guide').css({
      top: mouseY - brushSize / 2,
      left: mouseX - brushSize / 2,
    })

    // Do draw action
    fill(color)
    noStroke()
    ellipse(mouseX, mouseY, brushSize, brushSize)

    if (_lastMouseX && _lastMouseY) {
      stroke(color)
      strokeWeight(brushSize)
      line(_lastMouseX, _lastMouseY, mouseX, mouseY)
    }

    const data = {
      x1: (_lastMouseX * 100) / windowWidth || null,
      y1: (_lastMouseY * 100) / windowHeight || null,
      x2: (mouseX * 100) / windowWidth,
      y2: (mouseY * 100) / windowHeight,
      color: color,
      brushSize: brushSize,
    }

    socket.emit('draw', data)

    _lastMouseX = mouseX
    _lastMouseY = mouseY
    _payload.push(data)
  }
}

function mouseMoved() {
  // Only if mouse is on the canvas
  let hoverElement = document.elementFromPoint(mouseX, mouseY)
  if ((hoverElement && hoverElement.tagName !== 'CANVAS') || !hoverElement) {
    $('#guide').hide()
  } else {
    $('#guide')
      .css({
        top: mouseY - brushSize / 2,
        left: mouseX - brushSize / 2,
        width: brushSize,
        height: brushSize,
      })
      .show()
  }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight)
  background('#f6f6f6')
  socket.emit('resized')
}

function mouseReleased() {
  _lastMouseX = null
  _lastMouseY = null

  // Send the current payload
  if (_payload.length) {
    socket.emit('payload', _payload)
    _payload = []
  }
}

function keyReleased() {
  if (keyCode === 9) {
    // TAB pressed
    $('#message').focus()
  }

  // Ignore if typing
  if ($('input').is(':focus')) {
    return
  }

  // E pressed
  if (keyCode === 69) {
    if (!_lastColor) {
      _lastColor = color
      color = '#f6f6f6'
    } else {
      color = _lastColor
      _lastColor = null
    }

    $('#guide').css('border-color', color)
  }

  // C pressed
  if (keyCode === 67) {
    const imageData = this.drawingContext.getImageData(mouseX, mouseY, 1, 1)
    const pickedColor = imageData.data

    color = `rgb(${pickedColor[0]}, ${pickedColor[1]}, ${pickedColor[2]})`

    $('#guide').css('border-color', color)
  }
}

function mouseWheel(e) {
  if (e.delta < 0) {
    updateBrushSize(brushSize + 5)
  } else {
    updateBrushSize(brushSize - 5)
  }
  return false
}

function updateHeading(users, game) {
  if (!Object.keys(game).length) {
    $('#heading').html('Waiting for another player...')
    return
  } else if (typeof users[game.drawer] === 'undefined') {
    $('#heading').html(
      'Something went wrong, please wait for the next round...',
    )
    return
  }

  const pre =
    socket.id === game.drawer ? 'You are' : `${users[game.drawer].name} is`
  $('#heading').html(`<h2>${pre} drawing</h2>`)

  for (let i = 0; i < game.wordLength; i++) {
    let letter = $('<div>')
    letter.html('&nbsp;')
    letter.addClass('letter')
    if ($.inArray(i, game.blanks) > -1) letter.addClass('blank')
    $('#heading').append(letter)
  }
  for (let i = 0; i < game.hints.length; i++) {
    $('#heading .letter').eq(game.hints[i].index).html(game.hints[i].letter)
  }

  canDraw = false
  if (socket.id === game.drawer) canDraw = true
}

function updateBrushSize(size) {
  size = parseInt(size)

  if (size > brushSizeMax) {
    size = brushSizeMax
  } else if (size < brushSizeMin) {
    size = brushSizeMin
  }

  $('#brush-size').val(size)
  $('#guide').css({
    top: mouseY - size / 2,
    left: mouseX - size / 2,
    width: size,
    height: size,
  })

  brushSize = size
}

function updateUserStatus(id, status) {
  let userName = $(`#users .user-${id} .user-info`)
  userName.find('.status').remove()
  if (status) {
    userName.append(`<div class="status">${status}</div>`)
  }
}

function init() {
  showOverlay('init')

  $('#name').focus()

  if (name) {
    $('#name').val(name)
    $('#name').focus()
    $('#submit').show()
  }

  const initRenderHackIval = setInterval(() => {
    if (socket) {
      socket.emit('resized')
      clearInterval(initRenderHackIval)
    }
  }, 300)

  $(document).on('input', '#overlay input', () => {
    let name = $('#name').val()

    if (name.length >= 3 && name.length <= 20) {
      $('#submit').slideDown(100)
    } else {
      $('#submit').slideUp(100)
    }
  })

  $(document).on('click', '#submit', checkForm)
  $(document).on('keyup', '#name, #code', (e) => {
    if (e.keyCode === 13) checkForm()
  })

  $(document).on('click', '.overlay-close', hideOverlay)

  function checkForm() {
    let name = $('#name').val()
    let code = $('#code').length ? $('#code').val() : null

    if (name.length >= 3 && name.length <= 20) {
      Cookies.set('name', name)
      socket.emit('joined', name, code)

      /**
       * Methods that should be right after joining
       */
      $('#brush-size').attr({
        min: brushSizeMin,
        max: brushSizeMax,
        value: brushSize,
      })

      setInterval(() => {
        if (!socket.connected) {
          showOverlay('lost-connection')
        }
      }, 5000)

      hideOverlay()
    }
  }
}

function showOverlay(name) {
  $('.overlay-content').hide()
  $(`#overlay-${name}`).show()
  $('.hidden-by-overlay').hide()
  $('#overlay').fadeIn(100)
  canDraw = false
}

function hideOverlay() {
  $('#overlay').fadeOut(100, () => {
    $('.overlay-content').hide()
  })
  $('.hidden-by-overlay').fadeIn(100)
  canDraw = true
}

function sortUsersByPoints(users) {
  let sorted = []
  for (let id in users) {
    if (users.hasOwnProperty(id)) {
      sorted.push([id, users[id].points])
    }
  }

  sorted = sorted.sort((a, b) => b[1] - a[1])

  let userIds = []
  for (let i = 0; i < sorted.length; i++) {
    userIds.push(sorted[i][0])
  }

  return userIds
}

function plural(str, num, suffix) {
  suffix = suffix || 's'
  if (num !== 1) {
    return str + suffix
  }
  return str
}
