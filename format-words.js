const fs = require('fs')

// Get words
let words = require('./words.json')
words = words.map((word) => word.toLowerCase())

let result = []

// Filter duplicates
for (const word of words) {
  if (!result.includes(word)) {
    result.push(word)
  }
}

// Sort alphabetically
result = result.sort((a, b) => a < b ? -1 : 1)

// Make items human readable
// let resultJsonString = JSON.stringify(result)
let resultString = result.map(word => `  "${word}"`).join(',\n')

resultString = `[\n${resultString}\n]`

fs.writeFileSync('./words.json', resultString)
